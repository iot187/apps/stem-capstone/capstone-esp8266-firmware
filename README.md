## Capstone ESP8266 Firmware

This is a firmware for the esp8266 using the [esp8266 rtos sdk](https://github.com/espressif/ESP8266_RTOS_SDK) that
reads soil temperature
using [DS18B20 sensor](https://www.analog.com/media/en/technical-documentation/data-sheets/ds18b20.pdf)
and reads soil moisture using a probe the pass current through soil and measures its resistance.

The data read is then sent to a server to be stored in a database, then the database is accessed by grafana to plot a
graph of the data.

## Measuring Techniques & Errors

#### DS18B20 Temperature Sensor

- The DS18B20 temperature sensor has a configurable resolution from `9-bits` to `12-bits`. `12-bits` allow a precession
  of `0.0625°C`. (i.e. The minimum detectable change in temperature is `0.0625°C`).
- This is different from the accuracy of the sensor (i.e. How close the measured temperature to the actual temperature
  is) which is `±0.5°C` in the range `[-10°C, +85°C]`
- Since we are measuring the effect of **change** of temperature on soil, accuracy isn't that much important. Although
  the measured values are good enough to indicate the absolute measurements of temperature.
- The sensor has a measuring range from `[-55°C, +125°C]`

#### Moisture Sensor

- The moisture sensor consists of a fork-shaped probe that acts a potentiometer whose resistance varies depending on the
  soil water content. The more the water content the less the resistance.
- The probe is connected to a module that generates an output voltage on AO (Analog Output) depending on the resistance
  at the probe.
- The analog output is then sampled by `10-bit` ADC (analog to digital converter) which converts the voltage in the
  range `[0.0v, 3.3v]` to `[0, 1023]`
- From the previous information we can conclude that the moisture sensor resolution `3.3v / 1024 = 3.2mV`
- Lower numbers like `0` indicate a very high conductivity thus a high water content, on the other hand high values
  like `1023` indicate very high resistance thus a low water content, then the values measured are mapped
  to `[0% - 100%]` which indicates the soil water content.
- The moisture measurements are relative which means that the useful info obtained from the sensor is the **change** in
  water content (moisture) over time and not an absolute measurement of water content.

## Installation

Refer to the [esp8266 rtos sdk](https://github.com/espressif/ESP8266_RTOS_SDK) to download the sdk and the toolchain and
follow the steps to make a working environment.

```bash
# Ensure you have the toolchain installed
git clone https://github.com/espressif/ESP8266_RTOS_SDK.git ~/sdk/esp8266_rtos_sdk
git clone https://gitlab.com/iot187/apps/stem-capstone/capstone-esp8266-firmware ~/projects/capstone-esp8266-firmware
cd ~/projects/capstone-esp8266-firmware
export IDF_PATH=~/sdk/esp8266_rtos_sdk
export ESPPORT=/dev/ttyUSB0
export ESPBAUD=921600
cmake -B cmake-build-debug-xtensa -S . -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug-xtensa --parallel 12 --target flash
```

## Configuration

There is basically to modes of operation controlled by defining variables in the `constants.h` file:

1. define `AP_SSID`, `AP_PASS` and `DEFHOST` to connect to a specific network and a default host on which the server is
   listening.
2. define `AP_SSID` and `AP_PASS` to connect to a specific network which means the server will be listening on the
   gateway address.

## Usage

- Set a hotspot on a device configured with ssid and password as `AP_SSID` and `AP_PASS`
- Start capstone server on the same device
- Connect esp8266 to the power
- The blue light should flash every minute to indicate a measurement is taken and data is sent to the server.
