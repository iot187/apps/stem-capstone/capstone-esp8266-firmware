#pragma once

#define AP_SSID "Cap-Server-N"
#define AP_PASS "S3rv3r@0"
//#define DEFHOST "192.168.0.3"

ICACHE_FLASH_ATTR static constexpr unsigned char ENC_PASSWD[16] = {'*', 'W', '5', '&', '!', 'X', '8', 'X', '7', 'n',
                                                                   '^', 'i', 'f', 'P', '#', 'n'};

ICACHE_FLASH_ATTR static constexpr unsigned char MBEDTLS_DRBG_INIT[32] = {'t', 'g', 'F', 'g', 'm', 'z', 'K', 'y', 'M',
                                                                          'S', '4', 'p', '%', 'd', 'e', 'u', 'h', 'V',
                                                                          'g', '%', 'P', 'E', '*', 'm', 'H', 'q', 'J',
                                                                          'T', 'v', 'X', '$', 'b'};
