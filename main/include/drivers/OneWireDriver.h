#pragma once

#include <gpio.h>
#include <driver/gpio.h>
#include <esp8266/gpio_struct.h>
#include <esp_attr.h>
#include <FreeRTOS.h>
#include <task.h>

#include "utils.h"

#define GPIO_FAST_SET_LOW(pin) GPIO.out_w1tc |= (0x1 << pin)
#define GPIO_FAST_SET_HIGH(pin) GPIO.out_w1ts |= (0x1 << pin)
#define GPIO_FAST_GET_LEVEL(pin) ((GPIO.in >> pin) & 0x1)
#define GPIO_FAST_OUTPUT_ENABLE(pin) GPIO.enable_w1ts |= (0x1 << pin)
#define GPIO_FAST_OUTPUT_DISABLE(pin) GPIO.enable_w1tc |= (0x1 << pin)

#ifndef read_byte
#define read_byte(addr) (*(const uint8_t *)(addr))
#endif


namespace driver {
    class OneWire {
        ICACHE_FLASH_ATTR friend void swap(OneWire &lhs, OneWire &rhs) noexcept {
            using std::swap;
            swap(lhs.gpioPin_, rhs.gpioPin_);
        }

        auto writeBitHigh() noexcept {
            taskENTER_CRITICAL();
            GPIO_FAST_SET_LOW(gpioPin_);
            ets_delay_us(10);
            GPIO_FAST_SET_HIGH(gpioPin_);
            taskEXIT_CRITICAL();
            ets_delay_us(55);
        }

        auto writeBitLow() noexcept {
            taskENTER_CRITICAL();
            GPIO_FAST_SET_LOW(gpioPin_);
            ets_delay_us(65);
            GPIO_FAST_SET_HIGH(gpioPin_);
            taskEXIT_CRITICAL();
            ets_delay_us(5);
        }

        /**
         * @brief Writes a single bit on associated gpio pin
         * @param bit The bit to write on gpio pin
         */
        auto writeBit(uint8_t bit) {
            GPIO_FAST_OUTPUT_ENABLE(gpioPin_);
            if (bit & 0x1) {
                writeBitHigh();
            } else {
                writeBitLow();
            }
        }

        /**
         * @brief Reads a bit from the associated gpio pin
         * @return 1 if gpio pin reads high, 0 otherwise
         */
        IRAM_ATTR auto readBit() {
            uint8_t res;
            GPIO_FAST_OUTPUT_ENABLE(gpioPin_);
            taskENTER_CRITICAL();
            GPIO_FAST_SET_LOW(gpioPin_);
            ets_delay_us(3);
            GPIO_FAST_OUTPUT_DISABLE(gpioPin_);
            ets_delay_us(10);
            res = GPIO_FAST_GET_LEVEL(gpioPin_);
            taskEXIT_CRITICAL();
            ets_delay_us(53);
            return res;
        }

    public:
        /**
         * OneWire driver initialized over a specific pin using bit-banging technique
         * @param gpioPin The gpio pin to handle data tx and rx
         */
        explicit OneWire(gpio_num_t gpioPin) noexcept: gpioPin_(gpioPin) {
            const gpio_config_t gpioConfig{1u << gpioPin,
                                           GPIO_MODE_OUTPUT_OD,
                                           GPIO_PULLUP_ENABLE,
                                           GPIO_PULLDOWN_DISABLE,
                                           GPIO_INTR_DISABLE};
            espCall("Failed to set gpio config", gpio_config, &gpioConfig);
        }

        /**
         * @brief Deleted copy constructor
         */
        OneWire(const OneWire &oneWire) = delete;

        /**
         * @brief Move constructor
         */
        OneWire(OneWire &&oneWire) noexcept: OneWire(GPIO_NUM_MAX) {
            swap(*this, oneWire);
        }

        ~OneWire() noexcept = default;

        ICACHE_FLASH_ATTR OneWire &operator=(OneWire oneWire) noexcept {
            swap(*this, oneWire);
            return *this;
        }

        /**
         * @brief Initiates a reset sequence
         * @return true if a device signals its existence, false otherwise
         */
        auto reset() {
            bool res;
            GPIO_FAST_OUTPUT_ENABLE(gpioPin_);
            GPIO_FAST_SET_LOW(gpioPin_);
            ets_delay_us(480);
            GPIO_FAST_OUTPUT_DISABLE(gpioPin_);
            taskENTER_CRITICAL();
            ets_delay_us(70);
            res = GPIO_FAST_GET_LEVEL(gpioPin_);
            taskEXIT_CRITICAL();
            ets_delay_us(410);
            return !res;
        }

        /**
         * @brief Writes a byte on the associated gpio pin
         * @param byte Byte to send
         */
        void writeByte(uint8_t byte) {
            for (auto i{0u}; i < 8; ++i) {
                writeBit((byte & 1 << i) >> i);
            }
        }

        /**
         * @brief Reads a byte
         * @return The byte read
         */
        IRAM_ATTR auto readByte() {
            uint8_t res{0};
            for (auto i{0u}; i < 8; ++i) {
                res |= readBit() << i;
            }
            return res;
        }

        /**
         * @brief A convenience function to write bytes
         * @param bytes The vector of bytes to send
         */
        template<std::size_t Nm>
        IRAM_ATTR auto writeBytes(const std::array<uint8_t, Nm> &bytes) {
            for (auto byte: bytes)
                writeByte(byte);
        }

        /**
         * @brief A convenience function to read bytes
         * @param size The number of bytes to read
         * @return A vector of the bytes read
         */
        template<std::size_t Nm>
        IRAM_ATTR auto readBytes() {
            std::array<uint8_t, Nm> bytes;

            for (auto i{0}; i < Nm; ++i)
                bytes[i] = readByte();

            return bytes;
        }

        /**
         * @brief Issue a OneWire Protocol \b SKIP command
         */
        IRAM_ATTR auto skipRom() {
            writeByte(0xCC);
        }

        /**
         * @brief Issue a OneWire Protocol \b SEARCH command
         * @note conditional search is not implemented
         */
        IRAM_ATTR auto searchRom() {
            writeByte(0xF0);
        }

        /**
         * @brief Issue a OneWire Protocol \b SELECT command
         * @param address The address of the device
         */
        auto selectRom(std::uint64_t address) {
            writeByte(0x55);
            for (auto i{0u}; i < 64u; ++i) {
                writeBit(getBitVal(address, i));
            }
        }

        /**
         * @brief Reset the search state
         */
        IRAM_ATTR auto resetSearch() {
            address_ = 0;
            lastDiscrepancyIdx_ = -1;
        }

        /**
         * @brief Search for devices connected to the OneWire bus
         * @note It uses a DFS algorithm and each invocation will search for the next device
         * @return true if a device is found and have valid crc, false otherwise
         */
        IRAM_ATTR auto searchNext() {
            if (!reset()) {
                resetSearch();
                return false;
            }

            std::int8_t lastZeroIdx{-1};
            searchRom();
            for (std::int8_t bitIdx{0}; bitIdx < 64u; ++bitIdx) {
                auto bitVal{readBit()}, bitComplement{readBit()};
                if (bitVal & bitComplement) {           // bit and complement are both 1
                    resetSearch();
                    return false;
                } else if (bitVal ^ bitComplement) {    // bit and complement are different
                    if (bitVal == 1u)
                        setBitVal<1>(address_, bitIdx);
                    else
                        setBitVal<0>(address_, bitIdx);
                } else {                                // bit and complement are both 0
                    if (bitIdx > lastDiscrepancyIdx_) {
                        setBitVal<0>(address_, bitIdx);
                        lastZeroIdx = bitIdx;
                    } else if (bitIdx == lastDiscrepancyIdx_) {
                        setBitVal<1>(address_, bitIdx);
                    } else {
                        if (getBitVal(address_, bitIdx) == 0u)
                            lastZeroIdx = bitIdx;
                    }
                }

                writeBit(getBitVal(address_, bitIdx));
            }
            lastDiscrepancyIdx_ = lastZeroIdx;
            return true;
        }

        /**
         * @brief Get the address found after calling nextSearch
         * @note nextSearch must return true to ensure the address is valid
         * @return The address found during a previous search
         */
        ICACHE_FLASH_ATTR [[nodiscard]] auto getAddress() const {
            return address_;
        }

        ICACHE_FLASH_ATTR static std::uint8_t crc8(const std::uint8_t *addr, std::uint16_t len) {
            uint8_t crc = 0;
            while (len--) {
                std::uint8_t byte = *addr++;
                for (uint8_t i = 8; i; i--) {
                    uint8_t mix = (crc ^ byte) & 0x01;
                    crc >>= 1;
                    if (mix) crc ^= 0x8C;
                    byte >>= 1;
                }
            }
            return crc;
        }

    private:
        gpio_num_t gpioPin_;
        std::uint64_t address_{};
        std::int8_t lastDiscrepancyIdx_{-1};
    };
}