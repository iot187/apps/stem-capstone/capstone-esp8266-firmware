#pragma once

#include <string>
#include <vector>
#include <lwip/ip.h>
#include <lwip/netdb.h>
#include <tcpip_adapter.h>

#include "constants.h"
#include "mbedtls/GCM.h"

namespace http {
    class Client {
    public:
        ICACHE_FLASH_ATTR friend void swap(Client &lhs, Client &rhs) {
            using std::swap;
            swap(lhs.socketFd_, rhs.socketFd_);
            swap(lhs.aiFamily_, rhs.aiFamily_);
            swap(lhs.aiSockType_, rhs.aiSockType_);
            swap(lhs.aiProtocol_, rhs.aiProtocol_);
            swap(lhs.host_, rhs.host_);
            swap(lhs.sockLen_, rhs.sockLen_);
            swap(lhs.sockAddr_, rhs.sockAddr_);
        }

        /**
         * @brief Creates a client that connects to the target host
         * @note the host might be a domain or an ip4 address
         * @param host The host to connect to
         * @param port The port to connect to
         * @throws runtime_error if failed to create the socket
         */
        Client(const std::string &host, uint16_t port) : host_(host) {
            std::vector<int> vector;
            addrinfo hints{0, AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, nullptr, nullptr, nullptr};
            addrinfo *pAddrInfo;
            if (getaddrinfo(host.c_str(), std::to_string(port).c_str(), &hints, &pAddrInfo) != 0) {
                throw std::runtime_error("Failed to resolve host (" + host + ")");
            }
            memcpy(&sockAddr_, pAddrInfo->ai_addr, sizeof(sockaddr));
            aiFamily_ = pAddrInfo->ai_family;
            aiSockType_ = pAddrInfo->ai_socktype;
            aiProtocol_ = pAddrInfo->ai_protocol;
            sockLen_ = pAddrInfo->ai_addrlen;
            freeaddrinfo(pAddrInfo);
            if ((socketFd_ = socket(aiFamily_, aiSockType_, aiProtocol_)) < 0) {
                throw std::runtime_error("Failed to create socket");
            }
        }

        /**
         * @brief Copy constructor
         * @note The copied client will create its new unique socket
         */
        Client(const Client &client) : aiFamily_(client.aiFamily_),
                                       aiSockType_(client.aiSockType_), aiProtocol_(client.aiProtocol_),
                                       sockAddr_(client.sockAddr_), sockLen_(client.sockLen_) {
            if ((socketFd_ = socket(aiFamily_, aiSockType_, aiProtocol_)) < 0) {
                throw std::runtime_error("Failed to create socket");
            }
        }

        /**
         * @brief Move constructor
         */
        Client(Client &&client) noexcept {
            swap(*this, client);
        }

        /**
         * @brief The dtor closes the open socket
         */
        ~Client() {
            closesocket(socketFd_);
        };

        ICACHE_FLASH_ATTR Client &operator=(Client client) noexcept {
            swap(*this, client);
            return *this;
        }

        /**
         * @brief Send an HTTP Post request without reading response back
         * @param path The URL Path
         * @param headers Headers of the request
         * @param data Data to be sent
         */
        ICACHE_FLASH_ATTR void
        post(const std::string &path, const std::vector<std::string> &headers, const std::string &data) {
            std::string rawRequest;
            rawRequest += "POST " + path + " HTTP/1.1\r\n" + "HOST: " + host_ + "\r\n" + "Content-Length: " +
                          std::to_string(data.size()) + "\r\n";
            for (auto const &header: headers)
                rawRequest += header + "\r\n";
            rawRequest += "\r\n" + data;
            reliableSend(rawRequest);
        }

        ICACHE_FLASH_ATTR void
        postEncrypted(const std::string &path, const std::vector<std::string> &headers, const std::string &data) {
            std::vector<unsigned char> key(ENC_PASSWD, ENC_PASSWD + sizeof(ENC_PASSWD));
            std::vector<unsigned char> drbgInit(MBEDTLS_DRBG_INIT, MBEDTLS_DRBG_INIT + sizeof(MBEDTLS_DRBG_INIT));
            mbedtls::GCM gcm(MBEDTLS_CIPHER_ID_AES, key, mbedtls::CTR_DRBG(mbedtls::Entropy(), drbgInit));

            auto encryption{gcm.encrypt(data)};

            auto jsonReq{
                    std::unique_ptr<cJSON, std::function<decltype(cJSON_Delete)>>(cJSON_CreateObject(), cJSON_Delete)};
            cJSON_AddStringToObject(jsonReq.get(), "cipher", base64Encode(encryption.cipher).c_str());
            cJSON_AddStringToObject(jsonReq.get(), "iv", base64Encode(encryption.iv).c_str());
            cJSON_AddNumberToObject(jsonReq.get(), "tagSize", encryption.tagSize);
            post(path, headers, jsonEncode(jsonReq.get()));
        }

    private:
        /**
         * @breif A helper function that keeps trying to connect every 1 second unless a fatal error occur
         */
        ICACHE_FLASH_ATTR void reliableConnect() {
            while (connect(socketFd_, &sockAddr_, sockLen_) != 0) {
                switch (errno) {
                    case EBADF:
                    case ECONNREFUSED:
                    case ENETUNREACH:
                    case ETIMEDOUT:
                    case ECONNRESET:
                    case ENOTCONN:
                        closesocket(socketFd_);
                        if ((socketFd_ = socket(aiFamily_, aiSockType_, aiProtocol_)) < 0) {
                            espExceptionLog("Failed to create socket");
                            throw std::runtime_error("Failed to create socket");
                        }
                        vTaskDelay(1000 / portTICK_RATE_MS);
                        break;
                    default:
                        espExceptionLog("Failed to connect: " + std::string(strerror(errno)));
                        throw std::runtime_error("Failed to connect: " + std::string(strerror(errno)));
                }
            }
        }

        /**
         * A helper function the ensures data is sent unless a fatal error occur
         * @param rawRequest The data to be sent
         */
        ICACHE_FLASH_ATTR void reliableSend(const std::string &rawRequest) {
            auto sentData{0l};
            auto res{0l};
            while ((res = send(socketFd_, rawRequest.c_str() + sentData, rawRequest.size() - sentData, 0)) !=
                   rawRequest.size() - sentData) {
                if (res < 0) {
                    switch (errno) {
                        case ENOTCONN:
                        case ENOTSOCK:
                        case ENETDOWN:
                        case ENETUNREACH:
                        case ECONNRESET:
                            reliableConnect();
                            break;
                        case EBADF:
                            closesocket(socketFd_);
                            if ((socketFd_ = socket(aiFamily_, aiSockType_, aiProtocol_)) < 0) {
                                espExceptionLog("Failed to create socket");
                                throw std::runtime_error("Failed to create socket");
                            }
                            break;
                        default:
                            espExceptionLog("Failed to send data: " + std::string(strerror(errno)));
                            throw std::runtime_error("Failed to send data: " + std::string(strerror(errno)));
                    }
                } else {
                    sentData += res;
                }
            }
        }

        int socketFd_{};
        int aiFamily_{};
        int aiSockType_{};
        int aiProtocol_{};
        sockaddr sockAddr_{};
        socklen_t sockLen_{};
        std::string host_;
    };

}