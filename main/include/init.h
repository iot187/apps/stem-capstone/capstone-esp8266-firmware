#pragma once

#include <esp_wifi.h>

/**
 * @brief Initialize the wifi
 * @param wifiMode The wifi operating mode
 */
ICACHE_FLASH_ATTR void initializeWifi(wifi_mode_t wifiMode);