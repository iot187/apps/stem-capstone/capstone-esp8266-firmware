#pragma once

#include <vector>
#include <system_error>
#include <mbedtls/ctr_drbg.h>

#include "Entropy.h"

namespace mbedtls {
    class CTR_DRBG {
    public:
        ICACHE_FLASH_ATTR friend void swap(CTR_DRBG &lhs, CTR_DRBG &rhs) noexcept {
            using std::swap;
            swap(lhs.ctrDrbgContext_, rhs.ctrDrbgContext_);
        }

        explicit CTR_DRBG() : CTR_DRBG(mbedtls::Entropy()) {}

        explicit CTR_DRBG(mbedtls::Entropy entropy) : CTR_DRBG(std::move(entropy), {}) {}

        CTR_DRBG(mbedtls::Entropy entropy, const std::vector<unsigned char> &initialization) {
            mbedtls_ctr_drbg_init(&ctrDrbgContext_);
            if (mbedtls_ctr_drbg_seed(&ctrDrbgContext_, mbedtls_entropy_func,
                                      reinterpret_cast<void *>(&entropy.entropyContext), initialization.data(),
                                      initialization.size()) != 0) {
                espExceptionLog("Failed to seed random number generator");
                throw std::runtime_error("Failed to seed random number generator");
            }
        }

        CTR_DRBG(const CTR_DRBG &ctrDrbg) = delete;
        CTR_DRBG &operator=(const CTR_DRBG &ctrDrbg) = delete;

        CTR_DRBG(CTR_DRBG &&ctrDrbg)  noexcept {
            std::swap(ctrDrbg.ctrDrbgContext_, ctrDrbgContext_);
        }

        ~CTR_DRBG() noexcept {
            mbedtls_ctr_drbg_free(&ctrDrbgContext_);
        }

        ICACHE_FLASH_ATTR CTR_DRBG& operator=(CTR_DRBG ctrDrbg) noexcept {
            swap(*this, ctrDrbg);
            return *this;
        }

        ICACHE_FLASH_ATTR uint32_t getRandomInt() noexcept{
            uint32_t res;
            mbedtls_ctr_drbg_random(&ctrDrbgContext_, reinterpret_cast<unsigned char *>(&res), sizeof(res));
            return res;
        }

        ICACHE_FLASH_ATTR uint64_t getRandomLong() noexcept {
            uint64_t res;
            mbedtls_ctr_drbg_random(&ctrDrbgContext_, reinterpret_cast<unsigned char *>(&res), sizeof(res));
            return res;
        }

        ICACHE_FLASH_ATTR void fillRandomVector(std::vector<unsigned char> &vector) noexcept {
            mbedtls_ctr_drbg_random(&ctrDrbgContext_, vector.data(), vector.size());
        }

    private:
        mbedtls_ctr_drbg_context ctrDrbgContext_{};
    };
}