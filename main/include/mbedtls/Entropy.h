#pragma once

#include <mbedtls/entropy.h>
#include <algorithm>

namespace mbedtls {
    class Entropy {
    public:
        ICACHE_FLASH_ATTR friend void swap(Entropy &lhs, Entropy &rhs) noexcept {
            using std::swap;
            swap(lhs.entropyContext, rhs.entropyContext);
        }

        Entropy() noexcept {
            mbedtls_entropy_init(&entropyContext);
        }

        Entropy(const Entropy &entropy) = delete;

        Entropy(Entropy &&entropy) noexcept {
            swap(*this, entropy);
        }

        ~Entropy() noexcept {
            mbedtls_entropy_free(&entropyContext);
        }

        ICACHE_FLASH_ATTR Entropy &operator=(Entropy entropy) {
            swap(*this, entropy);
            return *this;
        }

        mbedtls_entropy_context entropyContext{};
    };
}