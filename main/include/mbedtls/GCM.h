#pragma once

#include <mbedtls/gcm.h>
#include <algorithm>

#include "CTR_DRBG.h"

namespace mbedtls {
    struct GCM_ENC {
        std::vector<unsigned char> cipher;
        std::vector<unsigned char> iv;
        unsigned int tagSize;
    };

    class GCM {
    public:
        ICACHE_FLASH_ATTR friend void swap(GCM &lhs, GCM &rhs) {
            using std::swap;
            swap(lhs.gcmContext_, rhs.gcmContext_);
        }

        GCM(mbedtls_cipher_id_t cipherId, std::vector<unsigned char> &key) : GCM(cipherId, key, CTR_DRBG()) {};

        explicit GCM(mbedtls_cipher_id_t cipherId, const std::vector<unsigned char> &key, CTR_DRBG ctrDrbg) : ctrDrbg_(
                std::move(ctrDrbg)) {
            mbedtls_gcm_init(&gcmContext_);
            if (key.size() != 16 && key.size() != 24 && key.size() != 32) {
                espExceptionLog("Key size must be 16, 24 or 32 only");
                throw std::runtime_error("Key size must be 16, 24 or 32 only");
            }
            mbedtls_gcm_setkey(&gcmContext_, cipherId, key.data(), key.size() * 8);
        }

        GCM(const GCM &gcm) = delete;

        GCM(GCM &&gcm) noexcept {
            swap(*this, gcm);
        }

        ~GCM() noexcept {
            mbedtls_gcm_free(&gcmContext_);
        }

        ICACHE_FLASH_ATTR GCM &operator=(GCM gcm) noexcept {
            swap(*this, gcm);
            return *this;
        }

        ICACHE_FLASH_ATTR auto encrypt(const std::string &data) -> GCM_ENC {
            static constexpr unsigned int tagSize = 8, ivSize = 32;
            std::vector<unsigned char> cipher(data.size() + tagSize), iv(ivSize), tag(tagSize);
            ctrDrbg_.fillRandomVector(iv);
            if (mbedtls_gcm_crypt_and_tag(&gcmContext_, MBEDTLS_ENCRYPT, data.size(), iv.data(), iv.size(), nullptr, 0,
                                          reinterpret_cast<const unsigned char *>(data.data()), cipher.data(),
                                          tag.size(), tag.data()) != 0) {
                throw std::runtime_error("Failed to encrypt data");
            }

            for (auto i{0ul}; i < tagSize; ++i)
                cipher[i + data.size()] = tag[i];
            return {std::move(cipher), std::move(iv), tagSize};
        }

    private:
        mbedtls_gcm_context gcmContext_{};
        CTR_DRBG ctrDrbg_;
    };
}