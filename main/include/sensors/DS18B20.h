#pragma once

#include <esp_attr.h>
#include <FreeRTOS.h>
#include <task.h>
#include "drivers/OneWireDriver.h"

#define CONVERT_T 0x44
#define WRITE_SCRATCHPAD 0x4E
#define READ_SCRATCHPAD 0xBE
#define COPY_SCRATCHPAD 0x48

#define SCRATCHPAD_SIZE 9
#define TEMP_LSB 0
#define TEMP_MSB 1
#define ALARM_HIGH 2
#define ALARM_LOW 3
#define CONFIGURATION 4
#define CRC8 8

namespace sensor {

    /**
     * @note incomplete implementation of DS18B20 sensor functions
     */
    class DS18B20 {
    public:
        enum TEMP_UNIT {
            CELSIUS, FAHRENHEIT, KELVIN
        };

        enum TEMP_RESOLUTION {
            RES_9_BIT = 9, RES_10_BIT = 10, RES_11_BIT = 11, RES_12_BIT = 12
        };

    private:
        friend void swap(DS18B20 &lhs, DS18B20 &rhs) {
            using std::swap;
            swap(lhs.oneWire_, rhs.oneWire_);
            swap(lhs.resolution_, rhs.resolution_);
        }

        std::array<std::uint8_t, SCRATCHPAD_SIZE> readScratchpad(std::uint64_t address = 0);

        void writeScratchpad(const std::array<uint8_t, SCRATCHPAD_SIZE> &scratchpad, std::uint64_t address = 0);

        /**
         * @brief Wait for the sensor to do the conversion of temperature
         */
        ICACHE_FLASH_ATTR void delayForConversion();

        /**
         * @brief Set the temperature resolution of sensors connected on the bust
         * @param resolution Resolution required
         * @param address If 0 set all connected sensors, otherwise target the specified address
         */
        ICACHE_FLASH_ATTR void setTempResolution(TEMP_RESOLUTION resolution, std::uint64_t address = 0);

    public:
        DS18B20(gpio_num_t gpioPin, TEMP_RESOLUTION resolution);

        DS18B20(DS18B20 &&ds18B20) noexcept;

        ICACHE_FLASH_ATTR DS18B20 &operator=(DS18B20 ds18B20);

        /**
         * @brief Send <b>Calculate Temperature</b> command to sensors on bus
         * @param address if 0 send to all connected sensors, otherwise target the specified address
         */
        ICACHE_FLASH_ATTR void calcTemp(std::uint64_t address = 0);

        /**
         * @brief Read temperature from sensor and convert it to the specified measuring unit
         * @note To update the value \b calcTemp must be called
         * @param temperatureUnit The measuring unit
         * @return temperature
         */
        ICACHE_FLASH_ATTR float readTemp(TEMP_UNIT temperatureUnit = CELSIUS);

    private:
        driver::OneWire oneWire_;
        TEMP_RESOLUTION resolution_;
    };
}
