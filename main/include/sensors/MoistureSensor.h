#pragma once

#include <adc.h>
#include <gpio.h>
#include <uart.h>
#include <cmath>

namespace sensor {
    class MoistureSensor {
        ICACHE_FLASH_ATTR friend void swap(MoistureSensor &lhs, MoistureSensor &rhs) {
            using std::swap;
            swap(lhs.powerGpio_, rhs.powerGpio_);
        }

    public:
        /**
         * A moisture sensor calibrated with a default range of dryness [1, 1023] mapped to [0%, 100%]
         * @note The moisture sensor uses analog pin
         */
        MoistureSensor() : MoistureSensor(1, 1023) {}

        /**
         * @brief A moisture sensor calibrated with a default range of dryness [1, 1023] mapped to [0%, 100%] and powered by
         * digital gpio pin to set the sensor on & off
         * @note the moisture sensor uses analog pin
         * @param powerGpio The digital pin controlling sensor power
         */
        explicit MoistureSensor(gpio_num_t powerGpio) : MoistureSensor(1, 1023, powerGpio) {}

        /**
         * @brief A moisture sensor calibrated with the provided range of dryness [valMin, valMax] mapped to [0%, 100%]
         * @note The moisture sensor uses analog pin
         * @note
         * The esp ADC has 10-bit resolution so input voltage [0.0v, 3.3v] is mapped to [1, 1023]. For this specific
         * sensor \b 1 means the soil is \b 100% wet and \b 1023 means the soil is \b 100% dry, but these values might
         * need calibration for each specific use case. For example we might want to say that 100 => 100% wet and 800 => 100%
         * @param valMin The minimum value to indicate 0% dryness (100% wetness)
         * @param valMax The maximum value to indicate 100% dryness (0% wetness)
         */
        MoistureSensor(uint16_t valMin, uint16_t valMax) : MoistureSensor(valMin, valMax, GPIO_NUM_MAX) {}

        /**
         * @brief A moisture sensor calibrated with the provided range of dryness [valMin, valMax] mapped to [0%, 100%]
         * @note The moisture sensor uses analog pin
         * @note
         * The esp ADC has 10-bit resolution so input voltage [0.0v, 3.3v] is mapped to [1, 1023]. For this specific
         * sensor \b 1 means the soil is \b 100% wet and \b 1023 means the soil is \b 100% dry, but these values might
         * need calibration for each specific use case. For example we might want to say that 100 => 100% wet and 800 => 100%
         * dry. This is configurable with the minVal and maxVal.
         * @param valMin The minimum value to indicate 0% dryness (100% wetness)
         * @param valMax The maximum value to indicate 100% dryness (0% wetness)
         * @param powerGpio The digital pin controlling sensor power
         */
        explicit MoistureSensor(uint16_t valMin, uint16_t valMax, gpio_num_t powerGpio) noexcept:
                valMin_(valMin), valMax_(valMax), powerGpio_(powerGpio) {
            adc_config_t adcConfig{ADC_READ_TOUT_MODE, 8};
            const gpio_config_t gpioConfig{1u << powerGpio,
                                           GPIO_MODE_OUTPUT,
                                           GPIO_PULLUP_DISABLE,
                                           GPIO_PULLDOWN_DISABLE,
                                           GPIO_INTR_DISABLE};
            espCall("Failed to init ADC", adc_init, &adcConfig);
            if (powerGpio != GPIO_NUM_MAX) {
                espCall("Failed to init power GPIO", gpio_config, &gpioConfig);
                espCall("Failed to set gpio level low", gpio_set_level, powerGpio, 0u);
            }
        }

        /**
         * A moisture sensor handling the same hardware as the one copied
         * @param moistureSensor The object to copy
         */
        MoistureSensor(const MoistureSensor &moistureSensor) noexcept = default;

        /**
         * A moisture sensor handling the same hardware as the one being moved
         * @param moistureSensor The object being moved
         */
        MoistureSensor(MoistureSensor &&moistureSensor) noexcept: MoistureSensor() {
            swap(*this, moistureSensor);
        }

        MoistureSensor &operator=(MoistureSensor moistureSensor) noexcept {
            swap(*this, moistureSensor);
            return *this;
        }

        /**
         * Reads moisture as a percent
         * @return A value in range [0.0, 100.0]
         */
        ICACHE_FLASH_ATTR auto read() noexcept {
            uint16_t val;
            if (powerGpio_ != GPIO_NUM_MAX) {
                gpio_set_level(powerGpio_, 1);
                vTaskDelay(20 / portTICK_RATE_MS);
                espCall("Failed to read ADC", adc_read, &val);
                gpio_set_level(powerGpio_, 0);
            } else {
                espCall("Failed to read ADC", adc_read, &val);
            }

            if (val == 0)
                return 0.0f;
            return std::round(map(val, valMax_, valMin_, 0.0f, 100.0f) * 1000.0f) / 1000.0f;
        }

        ICACHE_FLASH_ATTR auto readAverage(std::uint8_t times) noexcept {
            uint16_t val;
            auto accumulator{0.0f};
            if (powerGpio_ != GPIO_NUM_MAX) {
                gpio_set_level(powerGpio_, 1);
                vTaskDelay(20 / portTICK_RATE_MS);
                for (auto i{0u}; i < times; ++i) {
                    espCall("Failed to read ADC", adc_read, &val);
                    accumulator += std::round(map(val, valMax_, valMin_, 0.0f, 100.0f) * 1000.0f) / 1000.0f;
                }
                gpio_set_level(powerGpio_, 0);
            } else {
                for (auto i{0u}; i < times; ++i) {
                    espCall("Failed to read ADC", adc_read, &val);
                    accumulator += std::round(map(val, valMax_, valMin_, 0.0f, 100.0f) * 1000.0f) / 1000.0f;
                }
            }

            return std::round((accumulator / times) * 1000.0f) / 1000.0f;
        }
    private:
        uint16_t valMin_;
        uint16_t valMax_;
        gpio_num_t powerGpio_;
    };
}
