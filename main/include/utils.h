#pragma once

#include "constants.h"

#include <type_traits>
#include <memory>
#include <vector>

#include <cJSON.h>
#include <esp_log.h>
#include <lwip/inet.h>
#include <mbedtls/base64.h>
#include <tcpip_adapter.h>

#define espCall(errorMsg, function, ...) espCallImpl(__FILE__, __LINE__, errorMsg, function, ##__VA_ARGS__)
#define espExceptionLog(errorMsg) espExceptionLogImpl(__FILE__, __LINE__, errorMsg)

template<typename ESPFunction, typename ...Params>
ICACHE_FLASH_ATTR static inline auto
espCallImpl(const char *filename, uint_fast32_t line, const char *errorMsg, ESPFunction function,
            Params... params) -> typename std::enable_if<std::is_same<esp_err_t (*)(
        Params...), decltype(function)>::value, decltype(function(params...))>::type {
    auto ret{function(std::forward<Params>(params)...)};
    if (ret != ESP_OK) {
        ESP_LOGE("***ERROR***", "(%s, %u): %s\n\t%s\n", filename, line, esp_err_to_name(ret), errorMsg);
    }
    return ret;
}

template<typename ESPFunction, typename ...Params>
ICACHE_FLASH_ATTR static inline auto
espCallImpl(const char *filename, uint_fast32_t line, const char *errorMsg, ESPFunction function,
            Params... params) -> typename std::enable_if<std::is_same<void (*)(
        Params...), decltype(function)>::value, decltype(function(params...))>::type {
    function(std::forward<Params>(params)...);
}

ICACHE_FLASH_ATTR static inline auto
espExceptionLogImpl(const char *filename, uint_fast32_t line, const char *errorMsg) {
    ESP_LOGE("exception", "%s", errorMsg);
}

ICACHE_FLASH_ATTR static inline auto
espExceptionLogImpl(const char *filename, uint_fast32_t line, const std::string &errorMsg) {
    ESP_LOGE("exception", "%s", errorMsg.c_str());
}

ICACHE_FLASH_ATTR static inline constexpr auto isValidIp(uint32_t ipAddress) {
    return ((ipAddress & 0xff) == 192 || (ipAddress & 0xff) == 172 || (ipAddress & 0xff) == 10) &&
           ((ipAddress & 0xff << 8) >> 8) < 255 && ((ipAddress & 0xff << 16) >> 16) < 255 &&
           ((ipAddress & 0xff << 24) >> 24) < 255;
}

ICACHE_FLASH_ATTR static inline auto jsonEncode(const cJSON *item) {
    auto jsonPtr{cJSON_PrintUnformatted(item)};
    std::string json(jsonPtr);
    free(jsonPtr);
    return json;
}

ICACHE_FLASH_ATTR static inline auto jsonDecode(const std::string &json) {
    return std::unique_ptr<cJSON>(cJSON_Parse(json.c_str()));
}


ICACHE_FLASH_ATTR static inline auto base64Encode(const std::string &data) {
    size_t requireSize;
    std::string encodedStr;
    mbedtls_base64_encode(reinterpret_cast<unsigned char *>(encodedStr.data()), 0, &requireSize,
                          reinterpret_cast<const unsigned char *>(data.c_str()), data.size());
    encodedStr.resize(requireSize);
    mbedtls_base64_encode(reinterpret_cast<unsigned char *>(encodedStr.data()), encodedStr.size(), &requireSize,
                          reinterpret_cast<const unsigned char *>(data.c_str()), data.size());
    return encodedStr;
}

template<typename Tp, typename Alloc>
ICACHE_FLASH_ATTR static inline auto base64Encode(const std::vector<Tp, Alloc> &data) {
    return base64Encode(std::string(data.begin(), data.end()));
}

ICACHE_FLASH_ATTR static inline auto getGatewayIp() {
#ifdef DEFHOST
    return std::string(DEFHOST);
#else
    tcpip_adapter_ip_info_t ipInfo;
    if (espCall("Failed to get STA ip info", tcpip_adapter_get_ip_info, TCPIP_ADAPTER_IF_STA, &ipInfo) != ESP_OK) {
        return std::string("gateway");
    }
    char buf[16];
    ip4addr_ntoa_r(&ipInfo.gw, buf, sizeof(buf));
    return std::string(buf);
#endif
}

/**
 * @brief Maps a value known to be in specific range to another range
 * @tparam Tp1 Type of known range
 * @tparam Tp2 Type of target range
 * @param val The value to be mapped
 * @param valMin The minimum known bound
 * @param valMax The maximum known bound
 * @param mapMin The minimum target bound
 * @param mapMax The maximum target bound
 * @return The mapped value
 */
template<typename Tp1, typename Tp2>
ICACHE_FLASH_ATTR static constexpr inline auto
map(Tp1 val, Tp1 valMin, Tp1 valMax, Tp2 mapMin, Tp2 mapMax) -> std::enable_if_t<
        std::is_floating_point_v<Tp1> || std::is_integral_v<Tp1>, Tp2> {
    return (mapMax - mapMin) / (valMax - valMin) * (val - valMin) + mapMin;
}

/**
 * @brief Bounds a value between a min and max values
 * @tparam Tp Type of the values
 * @param val The value to be bound
 * @param min The minimum value
 * @param max The maximum value
 * @return value if between min and max inclusively, min or max depending on whatever bound violated
 */
template<typename Tp>
ICACHE_FLASH_ATTR static constexpr inline auto bound(Tp val, Tp min, Tp max) {
    if (val < min) return min;
    else if (val > max) return max;
    else return val;
}

/**
 * @brief Get the value of a bit shifted the provided times to the left
 * @tparam Tp1 Type of value
 * @tparam Tp2 Type of the shift value
 * @param val The value to query its bit value
 * @param shift The number of shifts to the left
 * @return the value of the bit at the required position
 */
template<typename Tp1, typename Tp2>
ICACHE_FLASH_ATTR static constexpr inline auto getBitVal(Tp1 val, Tp2 shift) -> std::uint8_t {
    return (val & static_cast<Tp1>(1) << shift) >> shift;
}

/**
 * @brief Set the value of a bit shifted the provided times to the left
 * @tparam bit The value of the bit
 * @tparam Tp1 Type of the value
 * @tparam Tp2 Type of the shift value
 * @param val The value to set its bit value
 * @param shift The number of shifts to the left
 * @note bit can only be 0 or 1
 */
template<std::uint8_t bit, typename Tp1, typename Tp2>
ICACHE_FLASH_ATTR static constexpr inline auto setBitVal(Tp1 &val, Tp2 shift) {
    if constexpr (bit == 0) {
        val &= ~(static_cast<Tp1>(1) << shift);
    } else if constexpr (bit == 1) {
        val |= static_cast<Tp1>(1) << shift;
    } else {
        static_assert(bit == 0 || bit == 1, "bit must be 0 or 1");
    }
}

/**
 * @brief Computes the average of results of a function after executing a specific amount of times
 * @tparam Tp Type of return value from the function
 * @tparam Params Type of parameters passed to the function
 * @param computeCount The number of times to compute the value
 * @param pFunction A pointer to the function to execute
 * @param params The parameters passed to the function
 * @return The average of the results of function
 */
template<typename Function, typename ...Params>
ICACHE_FLASH_ATTR static constexpr inline auto
computeAverage(std::uint8_t computeCount, Function function, Params ...params) {
    decltype(function(params...)) accumulator{};
    while (computeCount--) {
        accumulator += function(std::forward<Params>(params)...);
    }
    return accumulator / computeCount;
}