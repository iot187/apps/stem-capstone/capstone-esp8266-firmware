#include "init.h"
#include "utils.h"

#include <cstring>

#include <esp_event.h>
#include <esp_timer.h>
#include <nvs_flash.h>

ICACHE_FLASH_ATTR static void
wifiDisconnectEventHandler(void *eventHandler, esp_event_base_t eventBase, int32_t eventId, void *eventData) {
    espCall("Failed to connect to wifi", esp_wifi_connect);
}

ICACHE_FLASH_ATTR void initializeWifi(wifi_mode_t wifiMode) {
    wifi_init_config_t wifiInitConfig WIFI_INIT_CONFIG_DEFAULT();
    wifi_config_t wifiConfig{};
    memcpy(wifiConfig.sta.ssid, AP_SSID, sizeof(AP_SSID));
    memcpy(wifiConfig.sta.password, AP_PASS, sizeof(AP_PASS));
    wifiConfig.sta.bssid_set = false;
    wifiConfig.sta.scan_method = WIFI_ALL_CHANNEL_SCAN;
    espCall("Failed to initialize nvs flash required for wifi", nvs_flash_init);
    espCall("Failed to init wifi", esp_wifi_init, static_cast<const wifi_init_config_t *>(&wifiInitConfig));
    espCall("Failed to set wifi protocol", esp_wifi_set_protocol, ESP_IF_WIFI_STA,
            static_cast<uint8_t>(WIFI_PROTOCOL_11B | WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N));
    espCall("Failed to set wifi mode", esp_wifi_set_mode, wifiMode);
    espCall("Failed to set STA wifi config", esp_wifi_set_config, ESP_IF_WIFI_STA, &wifiConfig);
    espCall("Failed to start wifi", esp_wifi_start);
    espCall("Failed to connect to wifi", esp_wifi_connect);
    espCall("Failed to register wifi disconnect event handler", esp_event_handler_register, WIFI_EVENT,
            static_cast<int>(WIFI_EVENT_STA_DISCONNECTED), wifiDisconnectEventHandler, static_cast<void *>(nullptr));
}