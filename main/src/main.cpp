#include <vector>
#include <functional>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <esp_netif.h>
#include <gpio.h>
#include <cJSON.h>

#include "init.h"
#include "utils.h"
#include "http/Client.h"
#include "sensors/DS18B20.h"
#include "sensors/MoistureSensor.h"


ICACHE_FLASH_ATTR void startup(void *) {
    const gpio_config_t gpioConfig{1ull << GPIO_NUM_16,
                                   GPIO_MODE_OUTPUT,
                                   GPIO_PULLUP_DISABLE,
                                   GPIO_PULLDOWN_DISABLE,
                                   GPIO_INTR_DISABLE};
    espCall("Failed to set gpio config", gpio_config, &gpioConfig);

    try {
        http::Client httpClient(getGatewayIp(), 8080);
        sensor::MoistureSensor soilMoistureSensor(GPIO_NUM_5);
        sensor::DS18B20 soilTempSensor(GPIO_NUM_4, sensor::DS18B20::RES_12_BIT);

        while (true) {
            gpio_set_level(GPIO_NUM_16, 0);

            auto jsonData{
                    std::unique_ptr<cJSON, std::function<decltype(cJSON_Delete)>>(cJSON_CreateObject(), cJSON_Delete)};
            soilTempSensor.calcTemp();

            cJSON_AddNumberToObject(jsonData.get(), "st", soilTempSensor.readTemp(sensor::DS18B20::KELVIN));
            cJSON_AddNumberToObject(jsonData.get(), "sm", soilMoistureSensor.readAverage(5));
            httpClient.postEncrypted("/api/v1/data", {"Content-Type: application/json"}, jsonEncode(jsonData.get()));

            gpio_set_level(GPIO_NUM_16, 1);
            vTaskDelay(59000 / portTICK_RATE_MS);
        }
    } catch (const std::exception &exception) {
        espExceptionLog(exception.what());
    }
}

ICACHE_FLASH_ATTR static void
gotIpEventHandler(void *, esp_event_base_t, int32_t, void *) {
    xTaskCreate(startup, "startup", 8192, nullptr, 1, nullptr);
    esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, gotIpEventHandler);
}

extern "C" [[maybe_unused]] void app_main() {
    espCall("Failed to initialize network stack", esp_netif_init);
    espCall("Failed to create default event loop", esp_event_loop_create_default);
    initializeWifi(WIFI_MODE_STA);
    esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, gotIpEventHandler, nullptr);
}