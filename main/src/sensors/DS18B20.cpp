#include <array>
#include <cmath>

#include "drivers/OneWireDriver.h"
#include "sensors/DS18B20.h"


sensor::DS18B20::DS18B20(gpio_num_t gpioPin, DS18B20::TEMP_RESOLUTION resolution) : oneWire_(gpioPin), resolution_(resolution) {
    if (!oneWire_.reset()) {
        espExceptionLog("Failed to receive presence signal");
        throw std::runtime_error("Failed to receive presence signal");
    }
    setTempResolution(resolution);
}

std::array<std::uint8_t, SCRATCHPAD_SIZE> sensor::DS18B20::readScratchpad(std::uint64_t address) {
    std::array<uint8_t, SCRATCHPAD_SIZE> scratchPad{};
    do {
        if (!oneWire_.reset()) {
            espExceptionLog("Failed to receive presence signal");
            throw std::runtime_error("Failed to receive presence signal");
        }
        if (address == 0)
            oneWire_.skipRom();
        else
            oneWire_.selectRom(address);
        oneWire_.writeByte(READ_SCRATCHPAD);
        scratchPad = oneWire_.readBytes<SCRATCHPAD_SIZE>();
    } while (driver::OneWire::crc8(scratchPad.data(), 8) != scratchPad[CRC8]);
    return scratchPad;
}

void sensor::DS18B20::writeScratchpad(const std::array<uint8_t, 9> &scratchpad, std::uint64_t address) {
    if (!oneWire_.reset()) {
        espExceptionLog("Failed to receive presence signal");
        throw std::runtime_error("Failed to receive presence signal");
    }
    if (address == 0) {
        oneWire_.skipRom();
    } else {
        oneWire_.selectRom(address);
    }
    oneWire_.writeByte(WRITE_SCRATCHPAD);
    oneWire_.writeByte(scratchpad[ALARM_HIGH]);
    oneWire_.writeByte(scratchpad[ALARM_LOW]);
    oneWire_.writeByte(scratchpad[CONFIGURATION]);
    oneWire_.reset();
    if (address == 0)
        oneWire_.skipRom();
    else
        oneWire_.selectRom(address);
    oneWire_.writeByte(COPY_SCRATCHPAD);
}

void sensor::DS18B20::setTempResolution(sensor::DS18B20::TEMP_RESOLUTION resolution, std::uint64_t address) {
    auto scratchpad{readScratchpad(address)};
    switch (resolution) {
        case RES_9_BIT:
            scratchpad[CONFIGURATION] = 0x1F;
            break;
        case RES_10_BIT:
            scratchpad[CONFIGURATION] = 0x3F;
            break;
        case RES_11_BIT:
            scratchpad[CONFIGURATION] = 0x5F;
            break;
        case RES_12_BIT:
            scratchpad[CONFIGURATION] = 0x7F;
            break;
    }
    writeScratchpad(scratchpad, address);
}

sensor::DS18B20::DS18B20(sensor::DS18B20 &&ds18B20) noexcept: DS18B20(GPIO_NUM_MAX, TEMP_RESOLUTION::RES_12_BIT) {
    swap(*this, ds18B20);
}

sensor::DS18B20 &sensor::DS18B20::operator=(sensor::DS18B20 ds18B20) {
    swap(*this, ds18B20);
    return *this;
}

void sensor::DS18B20::calcTemp(std::uint64_t address) {
    if (!oneWire_.reset()) {
        espExceptionLog("Failed to receive presence signal");
        throw std::runtime_error("Failed to receive presence signal");
    }
    if (address == 0)
        oneWire_.skipRom();
    else
        oneWire_.selectRom(address);
    oneWire_.writeByte(CONVERT_T);
    delayForConversion();
}

float sensor::DS18B20::readTemp(sensor::DS18B20::TEMP_UNIT temperatureUnit) {
    auto scratchPad{readScratchpad()};
    auto lsb{scratchPad[TEMP_LSB]};
    auto msb{scratchPad[TEMP_MSB]};

    switch (resolution_) {
        case RES_9_BIT:
            lsb &= 0xF8;
            break;
        case RES_10_BIT:
            lsb &= 0xFC;
            break;
        case RES_11_BIT:
            lsb &= 0xFE;
            break;
        case RES_12_BIT:
            break;
    }

    auto raw{static_cast<uint16_t>((msb << 8) + lsb)};
    if (msb & 0x80)
        raw = ((raw ^ 0xffff) + 1) * -1;
    auto temp{std::round(static_cast<float>(raw / 16.0) * 10000) / 10000};

    switch (temperatureUnit) {
        case CELSIUS:
            return temp;
        case FAHRENHEIT:
            return temp * 9.0f / 5.0f + 32;
        case KELVIN:
            return temp + 273.15f;
    }
    espExceptionLog("Unknown temperature unit");
    throw std::runtime_error("Unknown temperature unit");
}

void sensor::DS18B20::delayForConversion() {
    switch (resolution_) {
        case RES_9_BIT:
            vTaskDelay(94 / portTICK_RATE_MS);
            break;
        case RES_10_BIT:
            vTaskDelay(188 / portTICK_RATE_MS);
            break;
        case RES_11_BIT:
            vTaskDelay(375 / portTICK_RATE_MS);
            break;
        case RES_12_BIT:
            vTaskDelay(750 / portTICK_RATE_MS);
            break;
    }
}
